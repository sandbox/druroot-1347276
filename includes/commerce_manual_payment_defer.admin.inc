<?php

/**
 * @file
 * Administrative forms for the Authorize.Net module.
 */


/**
 * Form callback: allows the user to complete a prior authorization.
 */
function commerce_manual_payment_defer_complete_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  $form['payment_recieved'] = array(
    '#markup' => t('I confirm that this payment has been recieved and accepted.'),
  );

  $form = confirm_form($form,
    t('What amount do you want to complete?'),
    'admin/commerce/orders/' . $order->order_id . '/payment',
    '',
    t('Confirm'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Validate handler: ensure a valid amount is given.
 *
 * There is no explicit validation required for this payment method.
 */
function commerce_manual_payment_defer_complete_form_validate($form, &$form_state) {

}

/**
 * Submit handler: process a prior authorization complete via AIM.
 */
function commerce_manual_payment_defer_complete_form_submit($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $order = $form_state['order'];

  drupal_set_message(t('Manual payment accepted.'));

  // Set the transaction status accordingly.
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;

  // Append a complete indication to the result message.
  $transaction->message = 'This payment has been recieved and accepted.<br />' . t('Completed: @date', array('@date' => format_date(REQUEST_TIME, 'short')));

  commerce_payment_transaction_save($transaction);

  // Set the order status accordingly.
  commerce_order_status_update($order, 'completed');

  $form_state['redirect'] = 'admin/commerce/orders';
}
